'use strict';

angular.module('app.tables', [])

    .controller('TablesCtrl', ['$http', function($http) {

        var controller = this;

        controller.ITEMS_PER_PAGE = 10;

        // data from files
        controller.filename = "file1.json";

        controller.items = [];
        controller.filteredItems = [];
        controller.pageItems = [];
        // filtering by search string
        controller.search = "";
        // sorting by columns
        controller.column = 'id';
        controller.reverse = false;
        // navigation between pages
        controller.buttons = [];
        controller.page = 1;

        controller.loadData = function () {
            // Some info for migrating to Angular 1.6 https://stackoverflow.com/a/26649568/5279156
            $http.get('data/' + controller.filename).success(function(data) {
                controller.items = data.data;
                controller.filterItems();
            });
        };
        controller.loadData();

        controller.filterItems = function () {
            controller.filteredItems = [];
            controller.buttons = [];
            controller.buttons.push(1);
            for (var i = 0; i < controller.items.length; i++) {
                if (controller.items[i].name.toLowerCase()
                        .indexOf(controller.search.toLowerCase()) !== -1) {
                    controller.filteredItems.push(controller.items[i]);
                    if (controller.filteredItems.length % controller.ITEMS_PER_PAGE === 0) {
                        controller.buttons.push(controller.filteredItems.length / controller.ITEMS_PER_PAGE + 1);
                    }
                }
            }
            controller.page = 1;
            controller.sortItems(null);
        };

        controller.sortItems = function (column) {
            if (column !== null) {
                controller.column = column;
                controller.reverse = (controller.column === column) ? !controller.reverse : true;
            }
            controller.filteredItems.sort(function(a, b) {
                if (a[controller.column] < b[controller.column]) {
                    return controller.reverse ? 1 : -1;
                }
                if (a[controller.column] > b[controller.column]) {
                    return controller.reverse ? -1 : 1;
                }
                return controller.column === 'id' ? 0 : (a.id - b.id) * (controller.reverse ? -1 : 1);
            });
            controller.setPage(controller.page);
        };

        controller.setPage = function (page) {
            controller.pageItems = [];
            controller.page = page;
            var minIndex = (page - 1) * controller.ITEMS_PER_PAGE;
            var maxIndex = minIndex + controller.ITEMS_PER_PAGE > controller.filteredItems.length ?
                controller.filteredItems.length : minIndex + controller.ITEMS_PER_PAGE;
            for (var i = minIndex; i < maxIndex; i++) {
                controller.pageItems.push(controller.filteredItems[i]);
            }
            controller.showOutput(null);
        };

        controller.showOutput = function (object) {
            controller.output = object === null ? '' : JSON.stringify(object);
        };
    }]);