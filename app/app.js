'use strict';

// Declare app level module which depends on views, and components
angular.module('app', [
    'ngRoute',
    'app.tables'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.when('/tables', {
        templateUrl: 'tables/index.html'
    })
        .otherwise({redirectTo: '/tables'});
}]);
